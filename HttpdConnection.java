package cod_5;

/*
Dopo aver introdotto il concetto di Thread nel package cod_4,
andremo ora a creare un programma che cattura e stampa sulla std-out
la richiesta HTTP-GET proveniente da un browser.
 */

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class HttpdConnection extends Thread {

    private final Socket client;

    public HttpdConnection(Socket s){
        this.client = s;

        initInstance();
        //facciamo semplicemente partire start() ma in un altro metodo.
        //In questo modo potremmo, eventualmente, estendere il metodo initInstance()
        //aggregando altre istruzioni (righe di codice).
    }

    private void initInstance(){
        // Ricorda che start() è un metodo che fa partire un thread e,
        // di conseguenza, ha bisogno obbligatoriamente dell'implementazione
        // del metodo run()
        start();
    }

    @Override
    public void run(){
        try{
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            DataOutputStream out = new DataOutputStream(client.getOutputStream());

            while(in.ready()) // ready() ritorna true se lo stream di input 'in' è pronto per essere letto.
            {
                String s = in.readLine();
                System.out.println("Ricevo: " + s);
                /*
                In questo caso sto salvando nella varibile s tutto ciò che, browser o software Postman, invia
                nello suo stream di uscita quando, tramite il metodo GET, richiede
                qualcosa all'host specificato. Noteremo in fatti come output tutto ciò
                che contiene il pacchetto dati (leggibile in caratteri).
                 */
            }
        }
        catch(IOException e){
            System.err.println("Errore durante la lettura delle richieste: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        finally{
            try {
                client.close();
            }
            catch(IOException e){
                System.err.println("Errore durante la chiusura della socket: " + e.getMessage());
                e.printStackTrace(System.err);
            }
        }
    }
}