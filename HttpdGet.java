package cod_5;
/*
Abbiamo bisogno di una porta nella quale la Socket sarà inzializzata.
Passiamo dunque una porta (es. 3000) nelle impostazioni di Run del proprio IDE.
 */

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;

public class HttpdGet {

    public static void main(String[] args) {

        if(args.length != 1){
            System.out.println("Sintassi: HttpdGet <porta>");
            System.exit(1);
        }

        ServerSocket server = null;

        try{
            server = new ServerSocket(Integer.parseInt(args[0]));
        }
        catch(IOException e){
            System.err.println("Errore durante la creazione di un server socket: " + e.getMessage());
            e.printStackTrace(System.err);
            System.exit(1);
        }
        catch(NumberFormatException e){
            System.err.println("Il parametro dell'argomento non è un numero valido per la porta. " + e.getMessage());
            e.printStackTrace(System.err);
            System.out.println("Sintassi: HttpdGet <porta>");

            System.exit(0); //metto 0 per distinguere le interruzioni del programma
        }

        try{
            InetAddress serverInfo = InetAddress.getLocalHost();
            System.out.println("Host info:");
            System.out.println(serverInfo.toString());
            System.out.println("PORT: " + server.getLocalPort());

            /*
            Il browser di default richiede prima il metodo GET.
            Suppporta anche il metodo POST; solo questi due.

            Pertanto per testare tale codice possiamo alternativamente usare
            Postman, tool per il testing delle API.
             */
            System.out.println(
                    "Richiedi il metodo HTTP-GET tramite l'url: " +
                    serverInfo.getHostAddress() +
                    ":" +
                    server.getLocalPort()
            + "\n");

            while(true){
                new HttpdConnection(server.accept());
            }
        }
        catch(UnknownHostException e){
            System.err.println("Errore durante la lettura dell'host-name: " + e.getMessage());
            e.printStackTrace(System.err);
        }
        catch(IOException e){
            System.err.println("Errore durante l'accettazione delle richieste: " + e.getMessage());
            e.printStackTrace(System.err);
        }
    }
}